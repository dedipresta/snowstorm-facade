organization := "com.dedipresta"

name := "snowstorm-facade"

version := "1.44.20131215"

scalaVersion := "2.12.8"

crossScalaVersions := Seq("2.11.12", "2.12.8")

enablePlugins(ScalaJSPlugin)

scalaJSUseMainModuleInitializer := false

libraryDependencies += "org.scala-js" %%% "scalajs-dom" % "0.9.2"

// publish

publishTo := Some("Artifactory Realm" at "https://www.dedipresta.com/artifactory/mvn-release-oss/")

credentials += Credentials(new File("credentials.properties"))

publishMavenStyle := true
