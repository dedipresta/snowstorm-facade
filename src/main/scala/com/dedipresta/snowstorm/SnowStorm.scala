package com.dedipresta.snowstorm

import org.scalajs.dom.raw.HTMLElement

import scala.scalajs.js
import scala.scalajs.js.annotation.JSGlobal
import scala.scalajs.js.{UndefOr, |}


@js.native
@JSGlobal("snowStorm")
object SnowStorm extends js.Object {


  /** Whether the snow should start automatically or not. */
  var autoStart: Boolean = js.native

  /** Snow is likely to be bad news for mobile phones' CPUs (and batteries.)
    *
    * Enable at your own risk.
    */
  var excludeMobile: Boolean = js.native

  /** Limit total amount of snow made (falling + sticking) */
  var flakesMax: Int = js.native

  /** Limit amount of snow falling at once
    *
    * (less = lower CPU use)
    */
  var flakesMaxActive: Int = js.native

  /** Theoretical "milliseconds per frame" measurement.
    *
    * 20 = fast + smooth, but high CPU use.
    * 50 = more conservative, but slower
    */
  var animationInterval: Int = js.native

  /** Enable transform-based hardware acceleration
    *
    * reduce CPU load
    */
  var useGPU: Boolean = js.native

  /** CSS class name for further customization on snow elements */
  var className: UndefOr[String] = js.native

  /** Integer for Y axis snow limit, 0 or null for "full-screen" snow effect */
  var flakeBottom: UndefOr[Int] = js.native

  /** Snow movement can respond to the user's mouse */
  var followMouse: Boolean = js.native

  /** Snow color. */
  var snowColor: String = js.native

  /** &bull; = bullet, &middot; is square on some systems etc. */
  var snowCharacter: String = js.native

  /** Whether or not snow should "stick" at the bottom.
    *
    * When off, will never collect
    */
  var snowStick: Boolean = js.native

  /** Element which snow will be appended to (null = document.body)
    *
    * can be an element ID eg. 'myDiv'
    * or a DOM node reference
    */
  var targetElement: UndefOr[HTMLElement | String] = js.native

  /** When recycling fallen snow (or rarely, when falling), have it "melt" and fade out if browser supports it */
  var useMeltEffect: Boolean = js.native

  /** Allow snow to randomly "flicker" in and out of view while falling */
  var useTwinkleEffect: Boolean = js.native

  /** true = snow does not shift vertically when scrolling.
    *
    * May increase CPU load, disabled by default
    * if enabled, used only where supported
    */
  var usePositionFixed: Boolean = js.native

  /** Whether to use pixel values for snow top/left vs. percentages.
    *
    * Auto-enabled if body is position:relative or targetElement is specified.
    */
  var usePixelPosition: Boolean = js.native

  /** Only snow when the window is in focus (foreground.) Saves CPU. */
  var freezeOnBlur: Boolean = js.native

  /** Left margin/gutter space on edge of container (eg. browser window.)
    *
    * Bump up these values if seeing horizontal scrollbars.
    */
  var flakeLeftOffset: Int = js.native

  /** Right margin/gutter space on edge of container */
  var flakeRightOffset: Int = js.native

  /** Max pixel width reserved for snow element */
  var flakeWidth: Int = js.native

  /** Max pixel height reserved for snow element */
  var flakeHeight: Int = js.native

  /** Maximum X velocity range for snow */
  var vMaxX: Int = js.native

  /** Maximum Y velocity range for snow */
  var vMaxY: Int = js.native

  /** CSS stacking order applied to each snowflake */
  var zIndex: Int = js.native



  /**  Sets the wind speed with a random value relative to vMaxX and vMaxY properties. */
  def randomizeWind(): Unit = js.native

  /** Stops the snow effect in place. */
  def freeze(): Unit = js.native

  /** Continues snowing from a "frozen" state. */
  def resume(): Unit = js.native

  /** Enables or disables the snow effect depending on state
    *
    * same as calling freeze() or resume()
    */
  def toggleSnow(): Unit = js.native

  /** Starts the snow */
  def start(): Unit = js.native

  /** Freezes and kills the snowstorm effect, and removes related event handlers.
    *
    * Snowstorm will not work properly if other methods are called after stop().
    */
  def stop(): Unit = js.native

}
