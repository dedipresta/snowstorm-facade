# Snowstorm facade

Scala.js facade for https://github.com/scottschiller/Snowstorm

## How to use

Start sbt

`sbt`

Publish the facade locally

`+publishLocal`

Add to your project

`libraryDependencies ++= Seq("com.dedipresta" %%% "snowstorm-facade" % "1.44.20131215")`

Load the JS then configure the plugin

```scala

    import com.dedipresta.snowstorm.SnowStorm
    
    // 1. Set a zIndex if required by your UI
    SnowStorm.zIndex = 1000
    // 2. Define a color for the snow
    SnowStorm.snowColor = "#ffffff"
    // 3. Define the max number of flakes that can be shown on screen at once
    SnowStorm.flakesMaxActive = 64
    // 4. Allow the snow to flicker in and out of the view
    SnowStorm.useTwinkleEffect = true
    // 5. Start the SnowStorm
    SnowStorm.start()
```
